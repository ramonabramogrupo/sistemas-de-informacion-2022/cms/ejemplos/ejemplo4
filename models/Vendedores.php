<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedores".
 *
 * @property int $IdVendedor
 * @property string|null $NombreVendedor
 * @property string|null $FechaAlta
 * @property string|null $NIF
 * @property string|null $FechaNac
 * @property string|null $Direccion
 * @property string|null $Poblacion
 * @property string|null $CodPostal
 * @property string|null $Telefon
 * @property string|null $EstalCivil
 * @property bool|null $activo
 *
 * @property Ventas[] $ventas
 */
class Vendedores extends \yii\db\ActiveRecord
{
    private string $fechaAltaCalculada;
    private string $fechaNacimiento;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FechaAlta', 'FechaNac'], 'safe'],
            [['activo'], 'boolean'],
            [['NombreVendedor', 'Direccion', 'Poblacion', 'Telefon'], 'string', 'max' => 50],
            [['NIF'], 'string', 'max' => 9],
            [['CodPostal'], 'string', 'max' => 5],
            [['EstalCivil'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdVendedor' => 'Codigo',
            'NombreVendedor' => 'Nombre',
            'FechaAlta' => 'Fecha Alta',
            'NIF' => 'NIF',
            'FechaNac' => 'Fecha Nacimiento',
            'Direccion' => 'Direccion',
            'Poblacion' => 'Poblacion',
            'CodPostal' => 'Codigo Postal',
            'Telefon' => 'Telefono',
            'EstalCivil' => 'Estado Civil',
            'activo' => 'Activo',
        ];
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['Cod Vendedor' => 'IdVendedor']);
    }
    
//    public function afterFind() {
//        parent::afterFind();
//        /** todo lo que realizo sobre los campos de una tabla antes de mostrarlos **/
//        //$this->FechaAlta="2022/01/01";
//        //$this->NombreVendedor= strtoupper($this->NombreVendedor);
//        return true;
//    }
    
    public function getFechaAltaCalculada(): string {
        if (!empty($this->FechaAlta)) {
            return Yii::$app->formatter->asDate($this->FechaAlta, 'php:d/m/Y');
        }else{
            return "";
        }
    }

    public function getFechaNacimiento(): string {
       if (!empty($this->FechaNac)) {
            return Yii::$app->formatter->asDate($this->FechaNac, 'php:d/m/Y');
        }else{
            return "";
        }
    }
   
}
