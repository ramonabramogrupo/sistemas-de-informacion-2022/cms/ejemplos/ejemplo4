<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IdProducto
 * @property string|null $NomProducto
 * @property int|null $IdGrupo
 * @property float|null $Precio
 * @property string|null $foto
 *
 * @property Grupos $idGrupo
 * @property Ventas[] $ventas
 */
class Productos extends \yii\db\ActiveRecord {

    private string $precioTotal;
    private string $fotoFinal;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['IdGrupo'], 'integer'],
            [['Precio'], 'number'],
            [['NomProducto'], 'string', 'max' => 50],
            [['foto'], 'string', 'max' => 100],
            [['IdGrupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::class, 'targetAttribute' => ['IdGrupo' => 'IdGrupo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'IdProducto' => 'Id Producto',
            'NomProducto' => 'Nom Producto',
            'IdGrupo' => 'Id Grupo',
            'Precio' => 'Precio',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[IdGrupo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdGrupo() {
        return $this->hasOne(Grupos::class, ['IdGrupo' => 'IdGrupo']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas() {
        return $this->hasMany(Ventas::class, ['Cod Producto' => 'IdProducto']);
    }

    public function getPrecioTotal(): string {
        if (isset($this->Precio)) {
            return $this->Precio . " €";
        } else {
            return "";
        }
    }

    public function getFotoFinal(): string {
        // compruebo si no tengo 
        if (empty($this->foto)) {
            return \yii\helpers\Html::img(
                            "@web/imgs/anonimo.png",
                            [
                                'class' => 'img-thumbnail d-block mx-auto my-2', // aspecto
                            ]
            );
        }

        return \yii\helpers\Html::img(
                        "@web/imgs/$this->IdProducto/$this->foto",
                        [
                            'class' => 'img-thumbnail d-block mx-auto my-2', // aspecto
                        ]
        );
    }

}
