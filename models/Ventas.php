<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $idventas
 * @property int $Cod Vendedor
 * @property int $Cod Producto
 * @property string $Fecha
 * @property float|null $Kilos
 *
 * @property Productos $codProducto
 * @property Vendedores $codVendedor
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Cod Vendedor', 'Cod Producto', 'Fecha'], 'required'],
            [['Cod Vendedor', 'Cod Producto'], 'integer'],
            [['Fecha'], 'safe'],
            [['Kilos'], 'number'],
            [['Cod Producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['Cod Producto' => 'IdProducto']],
            [['Cod Vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendedores::class, 'targetAttribute' => ['Cod Vendedor' => 'IdVendedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idventas' => 'Idventas',
            'Cod Vendedor' => 'Cod Vendedor',
            'Cod Producto' => 'Cod Producto',
            'Fecha' => 'Fecha',
            'Kilos' => 'Kilos',
        ];
    }

    /**
     * Gets query for [[CodProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProducto()
    {
        return $this->hasOne(Productos::class, ['IdProducto' => 'Cod Producto']);
    }

    /**
     * Gets query for [[CodVendedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodVendedor()
    {
        return $this->hasOne(Vendedores::class, ['IdVendedor' => 'Cod Vendedor']);
    }
    
    

}
