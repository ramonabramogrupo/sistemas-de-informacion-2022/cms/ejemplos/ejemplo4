<div class="row">
    <div class="col-lg-12">
        <h2>Codigo Producto: <?= $model->IdProducto ?><br></h2>
        <?= $model->fotoFinal ?>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->NomProducto ?></div>
        <div class="text-white bg-primary rounded p-2">Grupo:</div>
        <div class="p-1"><?= $model->IdGrupo ?></div>
        <div class="text-white bg-primary rounded p-2">Precio:</div>
        <div class="p-1"><?= $model->precioTotal ?></div>
        <?=
        yii\helpers\Html::a(
                //"ver", // texto del boton
                '<i class="fal fa-eye"></i>', // imagen del boton
                [
                    'view', // accion
                    'IdProducto' => $model->IdProducto // parametro
                ],
                [
                    "class" => "btn btn-success"
                ]
        ) // boton para ver el fabricante
        ?>

        <?=
        yii\helpers\Html::a(
                //"editar", // texto del boton
                '<i class="fad fa-pencil"></i>', //imagen del boton
                [
                    'update', // accion
                    'IdProducto' => $model->IdProducto // parametro
                ],
                [
                    "class" => "btn btn-success"
                ]
        ) // boton para modificar el fabricante
        ?>
        <?=
        yii\helpers\Html::a(
                //"eliminar", // texto del boton
                '<i class="fad fa-trash-alt"></i>', //imagen del boton
                [
                    'delete', // accion
                    'IdProducto' => $model->IdProducto // parametro
                ],
                [
                    "class" => "btn btn-danger",
                    'data' => [
                        'confirm' => '¿Estas seguro que deseas eliminar el fabricante?',
                        'method' => 'post',
                    ],
                ]
        ) // boton para eliminar el fabricante
        ?>
        <br class="float-none">
    </div>
</div>


