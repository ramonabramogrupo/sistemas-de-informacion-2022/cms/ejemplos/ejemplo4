<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = $model->IdProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IdProducto' => $model->IdProducto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IdProducto' => $model->IdProducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdProducto',
            'NomProducto',
            'IdGrupo',
            //'Precio',
            [
                'attribute' => 'Precio',
                'value' => function($model){
                    return $model->precioTotal;
                }
            ],
            //'foto',
            [
                'attribute' => 'foto',
                'format' => 'raw', // para dibuje imagen
                'value' => function($model){
                    return $model->fotoFinal;
                },
            ],     
        ],
    ]) ?>

</div>
