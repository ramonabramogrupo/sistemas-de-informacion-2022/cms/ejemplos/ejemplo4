<?php

use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=
        Html::a(
                '<i class="fal fa-address-card"></i> Tarjeta',
                ['index'],
                ['class' => 'btn btn-success'])
        ?>

        <?=
        Html::a(
                '<i class="fal fa-plus-circle"></i> Nuevo Producto',
                ['create'],
                ['class' => 'btn btn-success'])
        ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'IdProducto',
            'NomProducto',
            'IdGrupo',
            //'Precio',
            [
                'attribute' => 'Precio',
                'value' => function ($model) {
                    return $model->precioTotal;
                }
            ],
            //'foto',
            [
                'attribute' => 'foto',
                'format' => 'raw', // para dibuje imagen
                'value' => function ($model) {
                    return $model->fotoFinal;
                },
                'options' => [
                    'class' => 'col-3',
                ]
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Productos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdProducto' => $model->IdProducto]);
                }
            ],
        ],
    ]);
    ?>


</div>
