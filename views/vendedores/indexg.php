<?php

use app\models\Vendedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Gestion de Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                '<i class="far fa-address-card"></i> Tarjeta',  // etiqueta del boton
                ['index'], //accion del controlador
                ['class' => 'btn btn-primary'] // aspecto visual
            ) 
        ?>
        
        <?= Html::a(
                '<i class="fad fa-user-plus"></i> Nuevo Vendedor',  // etiqueta del boton
                ['create'], //accion del controlador
                ['class' => 'btn btn-primary'] // aspecto visual
            ) 
        ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdVendedor',
            'NombreVendedor',
            //'FechaAlta',
            //'fechaAltaCalculada',
            [
                'attribute' => 'FechaAlta',
                'value' => function($model){
                    return $model->fechaAltaCalculada; // nombre del campo calculado a mostrar
                }
            ],
            'NIF',
            //'FechaNac',
            //'fechaNacimiento',
             [
                'attribute' => 'FechaNac',
                'value' => function($model){
                    return $model->fechaNacimiento; // nombre del campo calculado a mostrar
                }
            ],
            //'Direccion',
            //'Poblacion',
            //'CodPostal',
            //'Telefon',
            //'EstalCivil',
            //'activo:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Vendedores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IdVendedor' => $model->IdVendedor]);
                 }
            ],
        ],
    ]); ?>


</div>
