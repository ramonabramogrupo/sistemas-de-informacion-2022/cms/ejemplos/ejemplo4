<?php

use app\models\Vendedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Gestion de Vendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                '<i class="fal fa-table"></i> Tabla',  // etiqueta del boton
                ['indexg'], //accion del controlador
                ['class' => 'btn btn-primary'] // aspecto visual
            ) 
        ?>
        
        <?= Html::a(
                '<i class="fad fa-user-plus"></i> Nuevo Vendedor',  // etiqueta del boton
                ['create'], //accion del controlador
                ['class' => 'btn btn-primary'] // aspecto visual
            ) 
        ?>
    </p>
    
    <?=
    \yii\widgets\ListView::widget([
        "dataProvider" => $dataProvider,
        "itemView" => "_ver",
        "itemOptions" => [
            'class' => 'col-lg-3 ml-auto mr-auto bg-light p-3 mb-5',
        ],
        "options" => [
            'class' => 'row',
        ],
        'layout' => "{items}{pager}",
    ])
    ?>

  

</div>
