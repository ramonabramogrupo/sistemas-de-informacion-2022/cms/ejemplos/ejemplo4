<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Vendedores $model */

$this->title = $model->IdVendedor;
$this->params['breadcrumbs'][] = ['label' => 'Vendedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vendedores-view">

    <h1>Mostrando los datos del vendedor <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'IdVendedor' => $model->IdVendedor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'IdVendedor' => $model->IdVendedor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro que deseas eliminar el vendedor?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdVendedor',
            'NombreVendedor',
            //'FechaAlta',
            //'fechaAltaCalculada',
            [
                'attribute' => 'FechaAlta',
                'value' => function($model){
                    return $model->fechaAltaCalculada; // nombre del campo calculado a mostrar
                }
            ],
            'NIF',
            //'FechaNac',
            //'fechaNacimiento',
            [
                'attribute' => 'FechaNac',
                'value' => function($model){
                    return $model->fechaNacimiento; // nombre del campo calculado a mostrar
                }
            ],                    
            'Direccion',
            'Poblacion',
            'CodPostal',
            'Telefon',
            'EstalCivil',
            'activo:boolean',
        ],
    ]) ?>

</div>
